﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using System.Data;

using MySql.Data.MySqlClient;
using Dapper;

using ZcbwComTwApi.Utils;


namespace ZcbwComTwApi.Models
{
    public class UserInfoModels
    {
        private MySqlConnection conn;
        private string sqlStr = string.Empty;

        public UserInfoModels(MySqlConnection _conn)
        {
            conn = _conn;
        }

        /// <summary>
        /// 取得使用者資訊
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="roles">角色 | 全部 | 超級使用者 | 中級使用者 | 普通使用者</param>
        /// <param name="status">帳號狀態</param>
        /// <param name="offset">頁碼</param>
        /// <param name="rowCount">筆數</param>
        /// <returns></returns>
        public IEnumerable<T> GetBaseUserInfo<T>(string roles, string status, int offset, int rowCount)
        {
            string strRolesWhereKey = (roles == "") ? "WHERE roles IS NOT NULL " : "WHERE roles = @roles ";
            string strStatusWhereKey = (status == "") ? "AND status IS NOT NULL " : "AND status = @status ";
            object wherekey = new { roles = roles, status = status };

            sqlStr = $@"SELECT userId,
                               userName,
                               account,
                               CASE roles
                                 WHEN 'supper' THEN '超級帳號'
                                 WHEN 'middle' THEN '中級帳號'
                                 ELSE '普通帳號'
                               END AS roles,
                               email,
                               phone,
                               status
                          FROM user
                            {strRolesWhereKey}
                            {strStatusWhereKey}
                            LIMIT {offset}, {rowCount}
            ";

            var result = conn.Query<T>(sqlStr, wherekey).ToList();

            return result;
        }

        /// <summary>
        /// 取得使用者詳細資料
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="roles">角色: 超級使用者 | 中級使用者 | 普通使用者</param>
        /// <param name="status">帳號狀態: 正常 | 停用</param>
        /// <param name="type">搜尋類別: 使用者名稱 | 帳號</param>
        /// <param name="offset">頁碼</param>
        /// <param name="rowCount">筆數</param>
        /// <returns></returns>
        public IEnumerable<T> GetUserInfoDetail<T>(string roles, string status, string type, int offset, int rowCount)
        {
            string strRolesWhereKey = (roles == "") ? "WHERE roles IS NOT NULL " : "WHERE roles = @roles ";
            string strStatusWhereKey = (status == "") ? "AND status IS NOT NULL " : "AND status = @status ";
            string strTypeWhereKey = (type == "") ? "AND (userName IS NOT NULL OR account IS NOT NULL) " : "AND (LOCATE(UPPER(@type), userName) OR LOCATE(UPPER(@type), account)) ";
            object wherekey = new { roles = roles, status = status, type = type };


            sqlStr = $@"SELECT userId,
                               userName,
                               account,
                               CASE roles
                                 WHEN 'supper' THEN '超級帳號'
                                 WHEN 'middle' THEN '中級帳號'
                                 ELSE '普通帳號'
                               END AS roles,
                               account,
                               email,
                               phone,
                               status
                          FROM user
                            {strRolesWhereKey}
                            {strStatusWhereKey}
                            {strTypeWhereKey}
                            LIMIT {offset}, {rowCount}
            ";

            var result = conn.Query<T>(sqlStr, wherekey).ToList();

            return result;
        }


        /// <summary>
        /// 查詢使用者資料總筆數
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="roles">角色: 超級使用者 | 中級使用者 | 普通使用者</param>
        /// <param name="status">帳號狀態: 正常 | 停用</param>
        /// <param name="type">搜尋類型: 使用者名稱 | 帳號</param>
        /// <returns></returns>
        public T GetUserInfoTotal<T>(string roles = "", string status = "", string type = "")
        {
            string strRolesWhereKey = (roles == "") ? "WHERE roles IS NOT NULL " : "WHERE roles = @roles ";
            string strStatusWhereKey = (status == "") ? "AND status IS NOT NULL " : "AND status = @status ";
            string strTypeWhereKey = (type == "") ? "AND (userName IS NOT NULL OR account IS NOT NULL) " : "AND (LOCATE(UPPER(@type), userName) OR LOCATE(UPPER(@type), account)) ";
            object wherekey = new { roles = roles, status = status, type = type };

            sqlStr = $@"SELECT COUNT(*) as total FROM user
                              {strRolesWhereKey}
                              {strStatusWhereKey}
                              {strTypeWhereKey}
            ";

            var result = conn.Query<T>(sqlStr, wherekey).FirstOrDefault();

            return result;
        }

        /// <summary>
        /// 取得唯一使用者資訊
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="userId">使用者 id</param>
        /// <returns></returns>
        public T GetOnlyUserInfo<T>(int userId)
        {
            sqlStr = @"SELECT userId, userName, roles, email, phone, status, mUser FROM user WHERE userId = @userId";

            object whereKey = new {
                userId = userId
            };

            var result = conn.Query<T>(sqlStr, whereKey).SingleOrDefault();
            
            return result;
        }

        /// <summary>
        /// 檢查使用者帳號是否重複
        /// </summary>
        /// <param name="account">帳號</param>
        /// <returns></returns>
        public T InspacetUserAccountExist<T>(string account)
        {
            sqlStr = @"SELECT account FROM user WHERE account = @account";

            object param = new {
                account = account
            };

            var result = conn.Query<T>(sqlStr, param).SingleOrDefault();

            return result;
        }

        /// <summary>
        /// 檢查使用者資訊存在
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="userId">使用者 id</param>
        /// <returns></returns>
        public T InspactUserInfoUserIdExist<T>(int userId)
        {
            sqlStr = "SELECT userId FROM user WHERE userId = @userId";

            object whereKey = new {
                userId = userId
            };

            var result = conn.Query<T>(sqlStr, whereKey).SingleOrDefault();

            return result;
        }

        /// <summary>
        /// 新增使用者資料
        /// </summary>
        /// <param name="obj">新增使用者資料模型</param>
        /// <returns></returns>
        public int InsertUserInfo(InsertUserInfoForm obj)
        {
            string userName = obj.userName;                                  //使用者名稱
            string account = obj.account;                                    //帳號
            string password = new ZcbwUtils().ZcbwShaEncrypt(obj.password);  //密碼
            string roles = obj.roles;                                        //平台角色
            string email = obj.email;                                        //信箱
            string phone = obj.phone;                                        //手機
            string status = obj.status;                                      //帳號狀態
            string nowUser = obj.nowUser;                                    //建立使用者與修改使用者
            string nowDate = DateTime.Now.ToString("yyyyMMdd");              //建立日期與修改日期
            string nowTime = DateTime.Now.ToString("HHmmss");                //建立時間與修改時間

            sqlStr = @"INSERT INTO user
                       ( userName,  account,  password,  roles,  email,  phone,  status, sUser,   sDate,  sTime,  mUser,  mDate,  mTime) VALUES
                       (@userName, @account, @password, @roles, @email, @phone, @status, @sUser, @sDate, @sTime, @mUser, @mDate, @mTime)
            ";

            object param = new {
                userName = userName,
                account = account,
                password = password,
                roles = roles,
                email = email,
                phone = phone,
                status = status,
                sUser = nowUser,
                sDate = nowDate,
                sTime = nowTime,
                mUser = nowUser,
                mDate = nowDate,
                mTime = nowTime
            };

            var rowCount = conn.Execute(sqlStr, param);

            return rowCount;
        }

        /// <summary>
        /// 更新使用者資訊
        /// </summary>
        /// <param name="obj">更新使用者資訊模型</param>
        /// <returns></returns>
        public int UpdateUserInfo(UpdateUserInfoForm obj)
        {
            int userId = obj.userId;                               //使用者 id
            string userName = obj.userName;                        //使用者名稱
            string roles = obj.roles;                              //平台角色
            string email = obj.email;                              //信箱
            string phone = obj.phone;                              //手機
            string status = obj.status;                            //狀態
            string nowUser = obj.mUser;                            //修改使用者
            string nowDate = DateTime.Now.ToString("yyyyMMdd");    //修改日期
            string nowTime = DateTime.Now.ToString("HHmmss");      //修改時間

            sqlStr = @"UPDATE user SET userName  = @userName,
                                       roles     = @roles,
                                       email     = @email,
                                       phone     = @phone,
                                       status    = @status,
                                       mUser     = @mUser,
                                       mDate     = @mDate,
                                       mTime     = @mTime
                                    WHERE userId = @userId
            ";

            object whereKey = new {
                userName = userName,
                roles = roles,
                email = email,
                phone = phone,
                status = status,
                mUser = nowUser,
                mDate = nowDate,
                mTime = nowTime,
                userId = userId
            };

            var rowCount = conn.Execute(sqlStr, whereKey);

            return rowCount;
        }

        /// <summary>
        /// 刪除使用者資訊
        /// </summary>
        /// <param name="userId"></param>
        /// <returns></returns>
        public int DeleteUserInfo(int userId)
        {
            sqlStr = "DELETE FROM user WHERE userId = @userId";

            object paramKey = new {
                userId = userId
            };

            var rowCount = conn.Execute(sqlStr, paramKey);

            return rowCount;
        }
    }

    /**
     * 接收模型
     */

    /// <summary>
    /// 查詢使用者表單，基本搜尋資料表單
    /// </summary>
    public class SearchUserInfoBaseForm
    {
        /// <summary>
        /// 平台角色
        /// </summary>
        [RegularExpression("(''|supper|middle|general){1,10}$")]
        public string roles { get; set; }
        
        /// <summary>
        /// 帳號狀態
        /// </summary>
        [RegularExpression("(''|1|0)")]
        public string status { get; set; }

        /// <summary>
        /// 頁碼
        /// </summary>
        [Required]
        [RegularExpression("[0-9]*$")]
        public int page { get; set; }

        /// <summary>
        /// 筆數
        /// </summary>
        [Required]
        [RegularExpression("(10|20|30|50|100)$")]
        public int itemBarMenu { get; set; }
    }

    /// <summary>
    /// 查詢使用者表單，詳細搜尋資料表單
    /// </summary>
    public class SearchUserInfoDetailForm
    {
        /// <summary>
        /// 平台角色
        /// </summary>
        [RegularExpression("(''|supper|middle|general){1,10}$")]
        public string roles { get; set; }
        
        /// <summary>
        /// 帳號狀態
        /// </summary>
        [RegularExpression("(''|1|0)")]
        public string status { get; set; }

        /// <summary>
        /// 搜尋類型
        /// </summary>
        [RegularExpression("(.*?)$")]
        public string type { get; set; }

        /// <summary>
        /// 頁碼
        /// </summary>
        [Required]
        [RegularExpression("[0-9]*$")]
        public int page { get; set; }

        /// <summary>
        /// 筆數
        /// </summary>
        [Required]
        [RegularExpression("(10|20|30|50|100)$")]
        public int itemBarMenu { get; set; }
    }

    /// <summary>
    /// 新增使用者資訊表單
    /// </summary>
    public class InsertUserInfoForm
    {
        /// <summary>
        /// 使用者名稱
        /// </summary>
        [Required]
        [RegularExpression("(.*?){3,6}$")]
        public string userName { get; set; }

        /// <summary>
        /// 平台角色
        /// </summary>
        [Required]
        [RegularExpression("(supper|middle|general){1,10}$")]
        public string roles { get; set; }

        /// <summary>
        /// 帳號
        /// </summary>
        [Required]
        [RegularExpression("[A-Za-z0-9]{8,50}$")]
        public string account { get; set; }

        /// <summary>
        /// 密碼
        /// </summary>
        [Required]
        [RegularExpression("[A-Za-z0-9]{8,50}$")]
        public string password { get; set; }

        /// <summary>
        /// 信箱
        /// </summary>
        [Required]
        [EmailAddress]
        [MaxLength(50)]
        public string email { get; set; }

        /// <summary>
        /// 手機
        /// </summary>
        [Required]
        [RegularExpression("[0-9]{10}$")]
        public string phone { get; set; }

        /// <summary>
        /// 帳號狀態
        /// </summary>
        [Required]
        [RegularExpression("(0|1){1}$")]
        public string status { get; set; }

        /// <summary>
        /// 建立者與修改者
        /// </summary>
        [Required]
        [RegularExpression("(.*?){3,6}$")]
        public string nowUser { get; set; }
    }

    /// <summary>
    /// 更新使用者資訊表單
    /// </summary>
    public class UpdateUserInfoForm
    {
        /// <summary>
        /// 使用者 id
        /// </summary>
        [Required]
        [RegularExpression("^[0-9]*$")]
        public int userId { get; set; }

        /// <summary>
        /// 使用者名稱
        /// </summary>
        [Required]
        [RegularExpression("(.*?){3,6}$")]
        public string userName { get; set; }

        /// <summary>
        /// 平台角色
        /// </summary>
        [Required]
        [RegularExpression("(supper|middle|general){1,10}$")]
        public string roles { get; set; }

        /// <summary>
        /// 信箱
        /// </summary>
        [Required]
        [EmailAddress]
        [MaxLength(50)]
        public string email { get; set; }

        /// <summary>
        /// 手機
        /// </summary>
        [Required]
        [RegularExpression("[0-9]{10}$")]
        public string phone { get; set; }

        /// <summary>
        /// 帳號狀態
        /// </summary>
        [Required]
        [RegularExpression("[0-1]{1}$")]
        public string status { get; set; }

        /// <summary>
        /// 修改者
        /// </summary>
        [Required]
        [RegularExpression("(.*?){3,6}$")]
        public string mUser { get; set; }
    }

    /// <summary>
    /// 查詢使用者 id
    /// </summary>
    public class SearchUserInfoUserIdForm
    {
        /// <summary>
        /// 使用者 id
        /// </summary>
        [Required]
        [RegularExpression("^[0-9]*$")]
        public int userId { get; set; }
    }
}