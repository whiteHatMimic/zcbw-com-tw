﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Net.Http.Headers;
using System.Web.Http.Cors;
using System.IO;

using MySql.Data.MySqlClient;
using ClosedXML.Excel;

using ZcbwComTwApi.Models;
using ZcbwComTwApi.Security;
using ZcbwComTwApi.VM;

namespace ZcbwComTwApi.Controllers
{
    public class TestApiController : ApiController
    {
        private MySqlConnection conn;

        ////[JwtAuthFilter]
        //[HttpGet]
        //[Route("api/ReportClassRoom/{classes?}/{gender?}/{name?}")]
        //[EnableCors(origins: "*", headers: "Authorization", methods: "GET")]
        //public HttpResponseMessage ReportClassRoom(string classes, string gender, string name)
        //{
        //    HttpResponseMessage response = new HttpResponseMessage();
        //    ResultModels result = new ResultModels();

        //    if (!ModelState.IsValid)
        //    {
        //        result.isSuccess = false;
        //        result.message = "資料傳輸錯誤";
        //        return Request.CreateResponse(HttpStatusCode.BadRequest, result);
        //    }

        //    try
        //    {
        //        using (conn = new MySqlConnection(Settings.MysqlSettings.connStr))
        //        {
        //            conn.Open();

        //            var classRoomList = new ClassRoomModles(conn).ReportClassRoom<ReportClassRoomVM>(classes, gender, name).ToList();

        //            //生成 excel
        //            XLWorkbook workbook = new XLWorkbook();
        //            IXLWorksheet classRoomReportTables = workbook.Worksheets.Add("班務清單報表"); //清增公做表

        //            string[] classRoomField = new string[] { "上課地點", "組別", "姓名", "性別", "班別", "所屬佛院", "所屬區別" };

        //            //新增班務欄位
        //            for (int i = 0; i < classRoomField.Length; i++)
        //            {
        //                classRoomReportTables.Cell(1, i + 1).Value = classRoomField[i];
        //            }

        //            //新增班務資料
        //            for (int i = 0; i < classRoomField.Length; i++)
        //            {
        //                for (int j = 0; j < classRoomList.Count(); j++)
        //                {
        //                    classRoomReportTables.Cell(i + 2, j + 1).Value = classRoomList[j].classLocation;
        //                    classRoomReportTables.Cell(i + 2, j + 2).Value = classRoomList[j].groupes;
        //                    classRoomReportTables.Cell(i + 2, j + 3).Value = classRoomList[j].name;
        //                    classRoomReportTables.Cell(i + 2, j + 4).Value = classRoomList[j].gender;
        //                    classRoomReportTables.Cell(i + 2, j + 5).Value = classRoomList[j].classes;
        //                    classRoomReportTables.Cell(i + 2, j + 6).Value = classRoomList[j].buddhaHall;
        //                    classRoomReportTables.Cell(i + 2, j + 7).Value = classRoomList[j].district;
        //                }
        //            }

        //            using (MemoryStream memoryStream = new MemoryStream())
        //            {
        //                workbook.SaveAs(memoryStream); //把做完的excel放到memoryStream裡
        //                response = new HttpResponseMessage(HttpStatusCode.OK);
        //                response.Content = new ByteArrayContent(memoryStream.ToArray());
        //                response.Content.Headers.ContentType = new MediaTypeHeaderValue("application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
        //                response.Content.Headers.ContentDisposition = new ContentDispositionHeaderValue("attachment");
        //                response.Content.Headers.ContentDisposition.FileName = $"test{ DateTime.Now}.xlsx";
        //                response.Content.Headers.ContentLength = memoryStream.Length; //這行會告知瀏覽器我們檔案的大小
        //                return response;
        //            }
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        result.isSuccess = false;
        //        result.message = ex.ToString();
        //        return response.RequestMessage.CreateResponse(HttpStatusCode.InternalServerError, result);
        //    }



        //    /*-----------------------------------------------------------------------------------------------------------------*/
        //    /*-----------------------------------------------------------------------------------------------------------------*/
        //    /*-----------------------------------------------------------------------------------------------------------------*/

        //    //var voteResult = new List<VoteResult>
        //    //{
        //    //    new VoteResult {Id=1,Option="java",VoteNumber=100 },
        //    //    new VoteResult {Id=2,Option="c#",VoteNumber=100 },
        //    //    new VoteResult {Id=3,Option="javascript",VoteNumber=120 },
        //    //    new VoteResult {Id=4,Option="ruby",VoteNumber=100 },
        //    //    new VoteResult {Id=5,Option="python",VoteNumber=100 },
        //    //    new VoteResult {Id=6,Option="go",VoteNumber=100 },
        //    //};

        //    //var workbook = new XLWorkbook();
        //    //var ws = workbook.Worksheets.Add("統計結果"); //新增頁籤1
        //    //ws.Cell(1, 1).Value = "選項";
        //    //ws.Cell(1, 2).Value = "得票數(票)";
        //    //ws.Cell(2, 1).Value = voteResult.Select(x => new { Option = x.Option, VoteNumber = x.VoteNumber });

        //    //var ipListResult = new List<IPList>
        //    //{
        //    //    new IPList {DeviceId="anson",IP="123.1.2.3",Option="java" },
        //    //    new IPList {DeviceId="anson1",IP="123.1.2.4",Option="c#" },
        //    //    new IPList {DeviceId="anson2",IP="123.1.2.5",Option="c#" },
        //    //    new IPList {DeviceId="anson3",IP="123.1.2.6",Option="javascript" },
        //    //    new IPList {DeviceId="anson4",IP="123.1.2.7",Option="javascript" },
        //    //    new IPList {DeviceId="anson5",IP="123.1.2.8",Option="ruby" },
        //    //    new IPList {DeviceId="anson6",IP="123.1.2.9",Option="go" },
        //    //};

        //    //var ws1 = workbook.Worksheets.Add("IP清單"); //新增頁籤1
        //    //ws1.Cell(1, 1).Value = "Device ID";
        //    //ws1.Cell(1, 2).Value = "IP";
        //    //ws1.Cell(1, 3).Value = "投票選項";
        //    //ws1.Cell(2, 1).Value = ipListResult;
        //    //ws.Columns().AdjustToContents();
        //    //ws1.Columns().AdjustToContents();

        //    //using (MemoryStream memoryStream = new MemoryStream())
        //    //{
        //    //    workbook.SaveAs(memoryStream); //把做完的excel放到memoryStream裡
        //    //    response = new HttpResponseMessage(HttpStatusCode.OK);
        //    //    response.Content = new ByteArrayContent(memoryStream.ToArray());
        //    //    response.Content.Headers.ContentType = new MediaTypeHeaderValue("application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
        //    //    response.Content.Headers.ContentDisposition = new ContentDispositionHeaderValue("attachment");
        //    //    response.Content.Headers.ContentDisposition.FileName = $"test{ DateTime.Now}.xlsx";
        //    //    response.Content.Headers.ContentLength = memoryStream.Length; //這行會告知瀏覽器我們檔案的大小
        //    //    return response;
        //    //}

        //}
    }
}
