﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Cors;

using MySql.Data.MySqlClient;

using ZcbwComTwApi.Models;
using ZcbwComTwApi.VM;
using ZcbwComTwApi.Security;

namespace ZcbwComTwApi.Controllers
{
    public class UserInfoApiController : ApiController
    {
        private MySqlConnection conn;

        /// <summary>
        /// 查詢使用者資訊，基本搜尋資料模式
        /// </summary>
        /// <param name="body">查詢使用者資訊，基本搜尋資料模型</param>
        /// <returns></returns>
        [JwtAuthFilter]
        [HttpPost]
        [Route("api/FetchUserInfoBase")]
        [EnableCors(origins: "*", headers: "Authorization", methods: "POST")]
        public HttpResponseMessage GetUserInfoBase(SearchUserInfoBaseForm body)
        {
            //創建新的統一回傳類型
            ResultModels result = new ResultModels();

            //檢查資料模型
            if (!ModelState.IsValid)
            {
                result.isSuccess = false;
                result.message = "資料傳輸錯誤";
                return Request.CreateResponse(HttpStatusCode.BadRequest, result);
            }

            try
            {
                string roles = body.roles == null ? "" : body.roles;
                string status = body.status == null ? "" : body.status;
                int offset = body.page * body.itemBarMenu - body.itemBarMenu;
                int rowCount = body.itemBarMenu;

                using (conn = new MySqlConnection(Settings.MysqlSettings.connStr))
                {
                    conn.Open();

                    //查詢使用者資訊基本搜尋
                    var userInfoList = new UserInfoModels(conn).GetBaseUserInfo<UserInfoListVM>(roles, status, offset, rowCount);

                    //查詢使用者資訊總數
                    var userInfoTotal = new UserInfoModels(conn).GetUserInfoTotal<UserInfoTotalVM>(roles, status);

                    //創建使用者資訊資料會診
                    UserInfoVM userInfo = new UserInfoVM();
                    userInfo.userInfo = userInfoList.ToList();
                    userInfo.total = userInfoTotal.total;

                    if (userInfoList.Count() == 0)
                    {
                        result.isSuccess = false;
                        result.message = "查無使用者資料";
                    }
                    else
                    {
                        result.isSuccess = true;
                        result.data = userInfo;
                    }
                }
            }
            catch(Exception ex)
            {
                result.isSuccess = false;
                result.message = ex.ToString();
                return Request.CreateResponse(HttpStatusCode.InternalServerError, result);
            }

            return Request.CreateResponse(HttpStatusCode.OK, result);
        }

        /// <summary>
        /// 查詢使用者資訊，詳細搜尋資料模式
        /// </summary>
        /// <param name="body">查詢使用者資訊，詳細搜尋資料模型</param>
        /// <returns></returns>
        [JwtAuthFilter]
        [HttpPost]
        [Route("api/FetchUserInfoDetail")]
        [EnableCors(origins: "*", headers: "Authorization", methods: "POST")]
        public HttpResponseMessage GetUserInfoDetail(SearchUserInfoDetailForm body)
        {
            //創建新的統一回傳類型
            ResultModels result = new ResultModels();

            //檢查資料模型
            if (!ModelState.IsValid)
            {
                result.isSuccess = false;
                result.message = "資料傳輸錯誤";
                return Request.CreateResponse(HttpStatusCode.BadRequest, result);
            }

            try
            {
                string roles = body.roles == null ? "" : body.roles;
                string status = body.status == null ? "" : body.status;
                string type = body.type == null ? "" : body.type;
                int offset = body.page * body.itemBarMenu - body.itemBarMenu;
                int rowCount = body.itemBarMenu;

                using (conn = new MySqlConnection(Settings.MysqlSettings.connStr))
                {
                    conn.Open();

                    //查詢使用者資訊詳細搜尋資料
                    var userInfoList = new UserInfoModels(conn).GetUserInfoDetail<UserInfoListVM>(roles, status, type, offset, rowCount);

                    //查詢使用者資訊總數
                    var userInfoTotal = new UserInfoModels(conn).GetUserInfoTotal<UserInfoTotalVM>(roles, status, type);

                    //組合資料
                    UserInfoVM userInfo = new UserInfoVM();
                    userInfo.userInfo = userInfoList.ToList();
                    userInfo.total = userInfoTotal.total;

                    if (userInfoList.Count() == 0)
                    {
                        result.isSuccess = false;
                        result.message = "查無使用者資料";
                    }
                    else
                    {
                        result.isSuccess = true;
                        result.data = userInfo;
                    }
                }
            }
            catch(Exception ex)
            {
                result.isSuccess = false;
                result.message = ex.ToString();
                return Request.CreateResponse(HttpStatusCode.InternalServerError, result);
            }

            return Request.CreateResponse(HttpStatusCode.OK, result);
        }

        /// <summary>
        /// 查詢一筆使用者資訊
        /// </summary>
        /// <param name="body">查詢使用者資訊 id 模型</param>
        /// <returns></returns>
        [JwtAuthFilter]
        [HttpPost]
        [Route("api/FetchOnlyUserInfo")]
        [EnableCors(origins: "*", headers: "Authorization", methods: "POST")]
        public HttpResponseMessage GetOnlyUserInfo(SearchUserInfoUserIdForm body)
        {
            //創建新的統一回傳類型
            ResultModels result = new ResultModels();

            //檢查資料模型
            if (!ModelState.IsValid)
            {
                result.isSuccess = false;
                result.message = "資料傳輸錯誤";
                return Request.CreateResponse(HttpStatusCode.BadRequest, result);
            }

            try
            {
                using (conn = new MySqlConnection(Settings.MysqlSettings.connStr))
                {
                    conn.Open();

                    //查詢一筆使用者資訊
                    var UserInfo = new UserInfoModels(conn).GetOnlyUserInfo<UserInfoOnlyVM>(body.userId);

                    if (UserInfo == null)
                    {
                        result.isSuccess = false;
                        result.message = "查無使用者資訊";
                        return Request.CreateResponse(HttpStatusCode.Forbidden, result);
                    }

                    //回傳資料
                    result.isSuccess = true;
                    result.data = UserInfo;
                }
            }
            catch (Exception ex)
            {
                result.isSuccess = false;
                result.message = ex.ToString();
                return Request.CreateResponse(HttpStatusCode.InternalServerError, result);
            }

            return Request.CreateResponse(HttpStatusCode.OK, result);
        }

        /// <summary>
        /// 新增使用者資訊
        /// </summary>
        /// <param name="body">新增使用者資訊模型</param>
        /// <returns></returns>
        [JwtAuthFilter]
        [HttpPost]
        [Route("api/CreateUserInfo")]
        [EnableCors(origins: "*", headers: "Authorization", methods: "POST")]
        public HttpResponseMessage InsertUserInfo(InsertUserInfoForm body)
        {
            //創建新的統一回傳類型
            ResultModels result = new ResultModels();

            //檢查資料模型
            if (!ModelState.IsValid)
            {
                result.isSuccess = false;
                result.message = "資料傳輸錯誤";
                return Request.CreateResponse(HttpStatusCode.BadRequest, result);
            }

            try
            {
                using (conn = new MySqlConnection(Settings.MysqlSettings.connStr))
                {
                    conn.Open();
                    MySqlTransaction transaction = conn.BeginTransaction();

                    //檢查使用者帳號是否重複
                    var userInfoAccountExist = new UserInfoModels(conn).InspacetUserAccountExist<UserInfoAccountVM>(body.account);

                    if (userInfoAccountExist != null)
                    {
                        result.isSuccess = false;
                        result.message = "此帳號已重複";
                        return Request.CreateResponse(HttpStatusCode.Forbidden, result);
                    }

                    //新增使用者資料
                    var insertRowUserInfo = new UserInfoModels(conn).InsertUserInfo(body);
                    
                    if (insertRowUserInfo == 1)
                    {
                        result.isSuccess = true;
                        result.message = "新增使用者資料成功";
                        transaction.Commit();
                    }
                    else
                    {
                        result.isSuccess = false;
                        result.message = "新增使用者資料失敗";
                        transaction.Rollback();
                    }
                }
            }
            catch(Exception ex)
            {
                result.isSuccess = false;
                result.message = ex.ToString();
                return Request.CreateResponse(HttpStatusCode.InternalServerError, result);
            }

            return Request.CreateResponse(HttpStatusCode.Created, result);
        }

        /// <summary>
        /// 更新使用者資訊
        /// </summary>
        /// <param name="body">更新使用者資訊模型</param>
        /// <returns></returns>
        [JwtAuthFilter]
        [HttpPatch]
        [Route("api/ReplaceUserInfo")]
        [EnableCors(origins: "*", headers: "Authorization", methods: "PATCH")]
        public HttpResponseMessage UpdateUserInfo(UpdateUserInfoForm body)
        {
            //創建新的統一回傳類型
            ResultModels result = new ResultModels();

            //檢查資料模型
            if (!ModelState.IsValid)
            {
                result.isSuccess = false;
                result.message = "資料傳輸錯誤";
                return Request.CreateResponse(HttpStatusCode.BadRequest, result);
            }

            try
            {
                using (conn = new MySqlConnection(Settings.MysqlSettings.connStr))
                {
                    conn.Open();
                    MySqlTransaction transaction = conn.BeginTransaction();

                    //檢查使用者資料是否存在
                    var isUserInfoUserIdExist = new UserInfoModels(conn).InspactUserInfoUserIdExist<UserInfoUserIdVM>(body.userId);

                    if (isUserInfoUserIdExist == null)
                    {
                        result.isSuccess = false;
                        result.message = "使用者已不存在";
                        return Request.CreateResponse(HttpStatusCode.Forbidden, result);
                    }

                    //更新使用者資料
                    var updateRowUserInfo = new UserInfoModels(conn).UpdateUserInfo(body);

                    if (updateRowUserInfo == 1)
                    {
                        result.isSuccess = true;
                        result.message = "更新使用者資料成功";
                        transaction.Commit();
                    }
                    else
                    {
                        result.isSuccess = false;
                        result.message = "更新使用者資料失敗";
                        transaction.Rollback();
                    }
                }
            }
            catch(Exception ex)
            {
                result.isSuccess = false;
                result.message = ex.ToString();
                return Request.CreateResponse(HttpStatusCode.InternalServerError, result);
            }

            return Request.CreateResponse(HttpStatusCode.OK, result);
        }

        /// <summary>
        /// 刪除使用者資訊
        /// </summary>
        /// <param name="userId">刪除使用者資訊 id</param>
        /// <returns></returns>
        [JwtAuthFilter]
        [HttpDelete]
        [Route("api/CancelUserInfo/{userId?}")]
        [EnableCors(origins: "*", headers: "Authorization", methods: "DELETE")]
        public HttpResponseMessage DeleteUserInfo(int userId)
        {
            //創建新的統一回傳類型
            ResultModels result = new ResultModels();

            //檢查資料模型
            if (!ModelState.IsValid)
            {
                result.isSuccess = false;
                result.message = "資料傳輸錯誤";
                return Request.CreateResponse(HttpStatusCode.BadRequest, result);
            }

            try
            {
                using (conn = new MySqlConnection(Settings.MysqlSettings.connStr))
                {
                    conn.Open();
                    MySqlTransaction transaction = conn.BeginTransaction();

                    //檢查使用者資料使否存在
                    var isUserInfoUserIdExist = new UserInfoModels(conn).InspactUserInfoUserIdExist<UserInfoUserIdVM>(userId);

                    if (isUserInfoUserIdExist == null)
                    {
                        result.isSuccess = false;
                        result.message = "使用者已不存在";
                        return Request.CreateResponse(HttpStatusCode.Forbidden, result);
                    }

                    //刪除使用者資料
                    var deleteRowUserInfo = new UserInfoModels(conn).DeleteUserInfo(userId);

                    if (deleteRowUserInfo == 1)
                    {
                        result.isSuccess = true;
                        result.message = "刪除使用者資料成功";
                        transaction.Commit();
                    }
                    else
                    {
                        result.isSuccess = false;
                        result.message = "刪除使用者資料失敗";
                        transaction.Rollback();
                    }
                }
            }
            catch(Exception ex)
            {
                result.isSuccess = false;
                result.message = ex.ToString();
                return Request.CreateResponse(HttpStatusCode.InternalServerError, result);
            }

            return Request.CreateResponse(HttpStatusCode.OK, result);
        }
    }
}
