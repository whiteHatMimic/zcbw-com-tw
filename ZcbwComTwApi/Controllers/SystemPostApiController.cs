﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Cors;

using MySql.Data.MySqlClient;
using ZcbwComTwApi.Models;
using ZcbwComTwApi.Security;
using ZcbwComTwApi.VM;

namespace ZcbwComTwApi.Controllers
{
    public class SystemPostApiController : ApiController
    {
        private MySqlConnection conn;

        /// <summary>
        /// 查詢系統公告，基本搜尋資料模式
        /// </summary>
        /// <param name="body">查詢系統公告資料，基本搜尋模型</param>
        /// <returns></returns>
        [JwtAuthFilter]
        [HttpPost]
        [Route("api/FetchSystemPostBase")]
        [EnableCors(origins: "*", headers: "Authorization", methods: "POST")]
        public HttpResponseMessage GetSystemPostBase(SearchSystemPostBaseForm body)
        {
            //創建新的統一回傳類型
            ResultModels result = new ResultModels();

            //創建新的統一回傳類型
            if (!ModelState.IsValid)
            {
                result.isSuccess = false;
                result.message = "資料傳輸錯誤";
                return Request.CreateResponse(HttpStatusCode.BadRequest, result);
            }

            try
            {
                string publish = body.publish == null ? "" : body.publish;    //發佈狀態
                int offset = body.page * body.itemBarMenu - body.itemBarMenu; //頁碼
                int itemBarMenu = body.itemBarMenu;                           //筆數

                using (conn = new MySqlConnection(Settings.MysqlSettings.connStr))
                {
                    conn.Open();

                    //查詢系統公告資料基本搜尋
                    var systemPostList = new SystemPostModels(conn).GetSystemPostBase<SystemPostListVM>(publish, offset, itemBarMenu);

                    //查詢系統公告總數
                    var systemPostTotal = new SystemPostModels(conn).GetSystemPostTotal<SystemPostTotalVM>(publish);

                    //創建系統公告資料會診
                    SystemPostVM systemPost = new SystemPostVM();
                    systemPost.systemPostInfo = systemPostList.ToList();
                    systemPost.total = systemPostTotal.total;

                    if (systemPostList.Count() == 0)
                    {
                        result.isSuccess = false;
                        result.message = "查無系統公告資料";
                    }
                    else
                    {
                        result.isSuccess = true;
                        result.data = systemPost;
                    }
                }
            }
            catch (Exception ex)
            {
                result.isSuccess = false;
                result.message = ex.ToString();
                return Request.CreateResponse(HttpStatusCode.InternalServerError, result);
            }

            return Request.CreateResponse(HttpStatusCode.OK, result);
        }

        /// <summary>
        /// 查詢系統公告資料，詳細搜尋模式
        /// </summary>
        /// <param name="body">查詢系統公告資料，詳細搜尋模型</param>
        /// <returns></returns>
        [JwtAuthFilter]
        [HttpPost]
        [Route("api/FetchSystemPostDetail")]
        [EnableCors(origins: "*", headers: "Authorization", methods: "POST")]
        public HttpResponseMessage GetSystemPostDetail(SearchSystemPostDetailForm body)
        {
            //創建新的統一回傳類型
            ResultModels result = new ResultModels();

            //檢查資料模型
            if (!ModelState.IsValid)
            {
                result.isSuccess = false;
                result.message = "資料傳輸錯誤";
                return Request.CreateResponse(HttpStatusCode.BadRequest, result);
            }

            try
            {
                string publish = body.publish == null ? "" : body.publish;    //發佈狀態
                string title = body.postTitle == null ? "" : body.postTitle;  //系統標題
                int offset = body.page * body.itemBarMenu - body.itemBarMenu; //頁碼
                int itemBarMenu = body.itemBarMenu;                           //筆數

                using (conn = new MySqlConnection(Settings.MysqlSettings.connStr))
                {
                    conn.Open();

                    //查詢系統公告詳細搜尋資料
                    var systemPostList = new SystemPostModels(conn).GetSystemPostDetail<SystemPostListVM>(publish, title, offset, itemBarMenu);

                    //查詢系統公告總數
                    var systemPostTotal = new SystemPostModels(conn).GetSystemPostTotal<SystemPostTotalVM>(publish, title);

                    //創建系統公告會診
                    SystemPostVM systemPost = new SystemPostVM();
                    systemPost.systemPostInfo = systemPostList.ToList();
                    systemPost.total = systemPostTotal.total;

                    if (systemPostList.Count() == 0)
                    {
                        result.isSuccess = false;
                        result.message = "查無公告資料";
                    }
                    else
                    {
                        result.isSuccess = true;
                        result.data = systemPost;
                    }
                }
            }
            catch(Exception ex)
            {
                result.isSuccess = false;
                result.message = ex.ToString();
                return Request.CreateResponse(HttpStatusCode.InternalServerError, result);
            }

            return Request.CreateResponse(HttpStatusCode.OK, result);
        }

        /// <summary>
        /// 查詢一筆系統公告
        /// </summary>
        /// <param name="body">查詢一筆系統公告模型</param>
        /// <returns></returns>
        [JwtAuthFilter]
        [HttpPost]
        [Route("api/FetchOnlySystemPost")]
        [EnableCors(origins: "*", headers: "Authorization", methods: "POST")]
        public HttpResponseMessage GetOnlySystemPost(SearchOnlySystemPostForm body)
        {
            //創建新的統一回傳類型
            ResultModels result = new ResultModels();

            //檢查資料模型
            if (!ModelState.IsValid)
            {
                result.isSuccess = false;
                result.message = "資料傳輸錯誤";
                return Request.CreateResponse(HttpStatusCode.BadRequest, result);
            }
            
            try
            {
                using (conn = new MySqlConnection(Settings.MysqlSettings.connStr))
                {
                    //查詢一筆公告資料
                    var systemPost = new SystemPostModels(conn).InspactSystemPost<OnlySystemPostVM>(body.systemPostId);

                    if (systemPost == null)
                    {
                        result.isSuccess = false;
                        result.message = "查無此筆系統公告資料";
                        return Request.CreateResponse(HttpStatusCode.Forbidden, result);    
                    }

                    //回傳資料
                    result.isSuccess = true;
                    result.data = systemPost;
                }
            }
            catch(Exception ex)
            {
                result.isSuccess = false;
                result.message = ex.ToString();
                return Request.CreateResponse(HttpStatusCode.InternalServerError, result);
            }

            return Request.CreateResponse(HttpStatusCode.OK, result);
        }

        /// <summary>
        /// 新增系統公告
        /// </summary>
        /// <param name="body">新增系統公告模型</param>
        /// <returns></returns>
        [JwtAuthFilter]
        [HttpPost]
        [Route("api/CreateSystemPost")]
        [EnableCors(origins: "*", headers: "Authorization", methods: "POST")]
        public HttpResponseMessage InsertSystemPost(InsertSystemPostForm body)
        {
            //創建新的統一回傳類型
            ResultModels result = new ResultModels();

            //檢查資料模型
            if (!ModelState.IsValid)
            {
                result.isSuccess = false;
                result.message = "資料傳輸錯誤";
                return Request.CreateResponse(HttpStatusCode.BadRequest, result);
            }

            try
            {
                using (conn = new MySqlConnection(Settings.MysqlSettings.connStr))
                {
                    conn.Open();
                    MySqlTransaction transaction = conn.BeginTransaction();

                    //新增系統公告
                    var insertRowSystemPost = new SystemPostModels(conn).InsertSystemPost(body);

                    if (insertRowSystemPost == 1)
                    {
                        result.isSuccess = true;
                        result.message = "新增系統公告資料成功";
                        transaction.Commit();
                    }
                    else
                    {
                        result.isSuccess = false;
                        result.message = "新增系統公告資料失敗";
                        transaction.Rollback();
                    }
                }
            }
            catch(Exception ex)
            {
                result.isSuccess = false;
                result.message = ex.ToString();
                return Request.CreateResponse(HttpStatusCode.InternalServerError, result);
            }

            return Request.CreateResponse(HttpStatusCode.Created, result);
        }

        /// <summary>
        /// 更新系統公告
        /// </summary>
        /// <param name="body">更新系統公告模型</param>
        /// <returns></returns>
        [JwtAuthFilter]
        [HttpPatch]
        [Route("api/ReplaceSystemPost")]
        [EnableCors(origins: "*", headers: "Authorization", methods: "PATCH")]
        public HttpResponseMessage UpdateSystemPost(UpdateSystemPostForm body)
        {
            //創建新的統一回傳類型
            ResultModels result = new ResultModels();

            //檢查資料模型
            if (!ModelState.IsValid)
            {
                result.isSuccess = false;
                result.message = "資料傳輸錯誤";
                return Request.CreateResponse(HttpStatusCode.BadRequest, result);
            }

            try
            {
                using (conn = new MySqlConnection(Settings.MysqlSettings.connStr))
                {
                    conn.Open();
                    MySqlTransaction transaction = conn.BeginTransaction();

                    //檢查系統公告是否存在
                    var isSystemPostExist = new SystemPostModels(conn).InspactSystemPost<OnlySystemPostVM>(body.systemPostId);

                    if (isSystemPostExist == null)
                    {
                        result.isSuccess = false;
                        result.message = "此系統公告已不存在";
                        return Request.CreateResponse(HttpStatusCode.Forbidden, result);
                    }

                    //更新系統公告資料
                    var updateRowSystemPost = new SystemPostModels(conn).UpdateSystemPost(body);

                    if (updateRowSystemPost == 1)
                    {
                        result.isSuccess = true;
                        result.message = "更新系統公告資料成功";
                        transaction.Commit();
                    }
                    else
                    {
                        result.isSuccess = false;
                        result.message = "更新系統公告資料失敗";
                        transaction.Rollback();
                    }
                }
            }
            catch(Exception ex)
            {
                result.isSuccess = false;
                result.message = ex.ToString();
                return Request.CreateResponse(HttpStatusCode.InternalServerError, result);
            }

            return Request.CreateResponse(HttpStatusCode.OK, result);
        }

        /// <summary>
        /// 更新系統公告發佈狀態
        /// </summary>
        /// <param name="body">更新系統公告發佈狀態模型</param>
        /// <returns></returns>
        [JwtAuthFilter]
        [HttpPatch]
        [Route("api/ReplaceSystemPostPublishStatus")]
        [EnableCors(origins: "*", headers: "Authorization", methods: "PATCH")]
        public HttpResponseMessage UpdateSystemPostStatus(UpdateSystemPostPublishStatusForm body)
        {
            //創建新的統一回傳類型
            ResultModels result = new ResultModels();

            //檢查資料模型
            if (!ModelState.IsValid)
            {
                result.isSuccess = false;
                result.message = "資料傳輸錯誤";
                return Request.CreateResponse(HttpStatusCode.BadRequest, result);
            }

            try
            {
                using (conn = new MySqlConnection(Settings.MysqlSettings.connStr))
                {
                    conn.Open();
                    MySqlTransaction transaction = conn.BeginTransaction();

                    //檢查系統公告是否存在
                    var isSystemPostExist = new SystemPostModels(conn).InspactSystemPost<OnlySystemPostVM>(body.systemPostId);

                    if (isSystemPostExist == null)
                    {
                        result.isSuccess = false;
                        result.message = "此系統公告已不存在";
                        return Request.CreateResponse(HttpStatusCode.Forbidden, result);
                    }

                    //更新系統公告資料發佈狀態
                    var updateRowSystemPostPublishStatus = new SystemPostModels(conn).UpdateSystemPostPublishStatus(body);

                    if (updateRowSystemPostPublishStatus == 1)
                    {
                        result.isSuccess = true;
                        result.message = "更新系統公告資料成功";
                        transaction.Commit();
                    }
                    else
                    {
                        result.isSuccess = false;
                        result.message = "更新系統公告資料失敗";
                        transaction.Rollback();
                    }
                }
            }
            catch(Exception ex)
            {
                result.isSuccess = false;
                result.message = ex.ToString();
                return Request.CreateResponse(HttpStatusCode.InternalServerError, result);
            }

            return Request.CreateResponse(HttpStatusCode.OK, result);
        }

        /// <summary>
        /// 刪除系統公告
        /// </summary>
        /// <param name="body">刪除系統公告 id</param>
        /// <returns></returns>
        [JwtAuthFilter]
        [HttpDelete]
        [Route("api/CancelSystemPost/{systemPostId?}")]
        [EnableCors(origins: "*", headers: "Authorization", methods: "DELETE")]
        public HttpResponseMessage DeleteSystemPost(int systemPostId)
        {
            //創建新的統一回傳類型
            ResultModels result = new ResultModels();

            //檢查資料模型
            if (!ModelState.IsValid)
            {
                result.isSuccess = false;
                result.message = "資料傳輸錯誤";
                return Request.CreateResponse(HttpStatusCode.BadRequest, result);
            }
            
            try
            {
                using (conn = new MySqlConnection(Settings.MysqlSettings.connStr))
                {
                    conn.Open();
                    MySqlTransaction transaction = conn.BeginTransaction();

                    //檢查系統公告是否存在
                    var isSystemPostExist = new SystemPostModels(conn).InspactSystemPost<OnlySystemPostVM>(systemPostId);

                    if (isSystemPostExist == null)
                    {
                        result.isSuccess = false;
                        result.message = "此系統公告已不存在";
                        return Request.CreateResponse(HttpStatusCode.Forbidden, result);
                    }

                    //刪除系統公告資料
                    var deleteRowSystemPost = new SystemPostModels(conn).DeleteSystemPost(systemPostId);

                    if (deleteRowSystemPost == 1)
                    {
                        result.isSuccess = true;
                        result.message = "刪除系統公告資料成功";
                        transaction.Commit();
                    }
                    else
                    {
                        result.isSuccess = false;
                        result.message = "刪除系統公告資料失敗";
                        transaction.Rollback();
                    }
                }
            }
            catch(Exception ex)
            {
                result.isSuccess = false;
                result.message = ex.ToString();
                return Request.CreateResponse(HttpStatusCode.InternalServerError, result);
            }

            return Request.CreateResponse(HttpStatusCode.OK, result);
        }
    }
}
