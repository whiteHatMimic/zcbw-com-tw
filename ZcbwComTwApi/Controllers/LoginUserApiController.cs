﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Collections.Specialized;
using System.Web;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Cors;

using MySql.Data.MySqlClient;

using ZcbwComTwApi.Utils;
using ZcbwComTwApi.Models;
using ZcbwComTwApi.VM;
using ZcbwComTwApi.Security;
using ZcbwComTwApi.Settings;

namespace ZcbwComTwApi.Controllers
{
    public class LoginUserApiController : ApiController
    {
        private MySqlConnection conn;

        /// <summary>
        /// 使用者登入
        /// </summary>
        /// <param name="body">查詢使用者帳號模型</param>
        /// <returns></returns>
        [HttpPost]
        [Route("api/LoginUser")]
        [EnableCors(origins: "*", headers: "*", methods: "POST")]
        public HttpResponseMessage LoginUser(SearchLoginUserForm body)
        {
            //創建新的統一回傳類型
            ResultModels result = new ResultModels();

            //檢查資料模型
            if (!ModelState.IsValid)
            {
                result.isSuccess = false;
                result.message = "資料傳輸錯誤";
                return Request.CreateResponse(HttpStatusCode.BadRequest);
            }

            try
            {
                string account = body.account;                                   //使用者帳號
                string password = new ZcbwUtils().ZcbwShaEncrypt(body.password); //使用者密碼，以 sha 加密

                using (conn = new MySqlConnection(Settings.MysqlSettings.connStr))
                {
                    conn.Open();
 
                    //檢查使用者帳號有無停用
                    var AccountStatus = new LoginUserModels(conn).InspactLoginUserApStatus<LoginAccountVM>(account, password);

                    if (AccountStatus != null)
                    {
                        result.isSuccess = false;
                        result.message = "此帳號密碼已停用";
                        return Request.CreateResponse(HttpStatusCode.BadRequest, result);
                    }

                    //查詢有無此筆帳號
                    var loginUserInfo = new LoginUserModels(conn).GetLoginUserInfo<LoginUserInfoVM>(account, password);

                    if (loginUserInfo == null)
                    {
                        result.isSuccess = false;
                        result.message = "帳號密碼無效，請確認帳號密碼無誤";
                        return Request.CreateResponse(HttpStatusCode.BadRequest, result);
                    }

                    //更新使用者登入日期、時間
                    var updateLoginDataTime = new LoginUserModels(conn).UpdateLoginUser(account, password);

                    //新 jwt 加密
                    JwtAuthUtil jwtAuthUtil = new JwtAuthUtil();

                    //創建 loginUser 使用者登入 jwt token 加密回傳
                    LoginUserInfoTokenVM loginUserInfoToken = new LoginUserInfoTokenVM
                    {
                        token = jwtAuthUtil.GenerateToken(loginUserInfo.userName, loginUserInfo.roles)
                    };

                    //回傳資料
                    result.isSuccess = true;
                    result.data = loginUserInfoToken;
                }
            }
            catch(Exception ex)
            {
                result.isSuccess = false;
                result.message = ex.ToString();
                return Request.CreateResponse(HttpStatusCode.InternalServerError, result);
            }

            return Request.CreateResponse(HttpStatusCode.OK, result);
        }
    }
}
