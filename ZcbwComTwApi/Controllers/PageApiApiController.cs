﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

using MySql.Data.MySqlClient;
using ZcbwComTwApi.Security;
using ZcbwComTwApi.Models;
using ZcbwComTwApi.VM;
using ZcbwComTwApi.Settings;

namespace ZcbwComTwApi.Controllers
{
    public class PageApiApiController : ApiController
    {
        private MySqlConnection conn;

        [JwtAuthFilter]
        [HttpPost]
        [Route("api/GetPage")]
        public HttpResponseMessage GetEntirePageList()
        {
            ResultModels result = new ResultModels();
            
            try
            {
                using (conn = new MySqlConnection(Settings.MysqlSettings.connStr))
                {
                    var PageEntireList = new PageModels(conn).GetEntirePageList<string>();

                    if (PageEntireList.Count() == 0)
                    {
                        result.isSuccess = false;
                        result.message = "查無頁面";
                    }
                    else
                    {
                        result.isSuccess = true;
                        result.data = PageEntireList;
                    }
                }
            }
            catch(Exception ex)
            {
                result.isSuccess = false;
                result.message = ex.ToString();
                Request.CreateResponse(HttpStatusCode.InternalServerError, result);
            }

            return Request.CreateResponse(HttpStatusCode.OK, result);
        }
    }
}
