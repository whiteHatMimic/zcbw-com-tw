﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ZcbwComTwApi.VM
{
    //傳
    public class UserInfoVM
    {
        public int total { get; set; }
        public List<UserInfoListVM> userInfo { get; set; }
    }

    public class UserInfoListVM
    {
        public string userId { get; set; }
        public string userName { get; set; }
        public string roles { get; set; }
        public string account { get; set; }
        public string email { get; set; }
        public string phone { get; set; }
        public string status { get; set; }
    }

    public class UserInfoTotalVM
    {
        public int total { get; set; }
    }

    public class UserInfoAccountVM
    {
        public string account { get; set; }
    }

    public class UserInfoUserIdVM
    {
        public string userId { get; set; }
    }

    public class UserInfoOnlyVM
    {
        public string userId { get; set; }
        public string userName { get; set; }
        public string roles { get; set; }
        public string email { get; set; }
        public string phone { get; set; }
        public string status { get; set; }
        public string mUser { get; set; }
    }
}