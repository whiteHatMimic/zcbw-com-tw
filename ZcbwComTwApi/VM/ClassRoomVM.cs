﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ZcbwComTwApi.VM
{
    /// <summary>
    /// 班務清單合併檢閱模型
    /// </summary>
    public class ClassRoomVM
    {
        public int total { get; set; }
        public List<ClassRoomListVM> classRoom { get; set; }
    }

    /// <summary>
    /// 班務清單檢閱模型
    /// </summary>
    public class ClassRoomListVM
    {
        /// <summary>
        /// 班務 id
        /// </summary>
        public string classRoomId { get; set; }
        
        /// <summary>
        /// 上課地點
        /// </summary>
        public string classLocation { get; set; }

        /// <summary>
        /// 組別
        /// </summary>
        public string groupes { get; set; }

        /// <summary>
        /// 姓名
        /// </summary>
        public string name { get; set; }

        /// <summary>
        /// 性別
        /// </summary>
        public string gender { get; set; }

        /// <summary>
        /// 班別
        /// </summary>
        public string classes { get; set; }

        /// <summary>
        /// 所屬佛院
        /// </summary>
        public string buddhaHall { get; set; }

        /// <summary>
        /// 所屬區別
        /// </summary>
        public string district { get; set; }
    }

    /// <summary>
    /// 班務數量檢閱模型
    /// </summary>
    public class ClassRoomTotalVM
    {
        public int total { get; set; }
    }

    /// <summary>
    /// 檢查班務組別重複檢閱模型
    /// </summary>
    public class ClassRoomClassesVM
    {
        /// <summary>
        /// 姓名
        /// </summary>
        public string name { get; set; }

        /// <summary>
        /// 班別
        /// </summary>
        public string classes { get; set; }

        /// <summary>
        /// 組別
        /// </summary>
        public string groups { get; set; }
    }

    /// <summary>
    /// 檢查班務 id 存在檢閱模型
    /// </summary>
    public class ClassRoomClassRoomIdVM
    {
        /// <summary>
        /// 班務 id
        /// </summary>
        public int classRoomId { get; set; }
    }

    /// <summary>
    /// 取得一筆班務資料檢閱模型
    /// </summary>
    public class OnlyOneClassRoomVM
    {
        public int classRoomId { get; set; }
        public string classLocation { get; set; }
        public string groupes { get; set; }
        public string groupNo { get; set; }
        public string serialNo { get; set; }
        public string name { get; set; }
        public string gender { get; set; }
        public string classes { get; set; }
        public string buddhaHall { get; set; }
        public string district { get; set; }
        public string mUser { get; set; }
    }

    /// <summary>
    /// 匯出報表資料
    /// </summary>
    public class ReportClassRoomVM
    {
        /// <summary>
        /// 上課地點
        /// </summary>
        public string classLocation { get; set; }

        /// <summary>
        /// 組別
        /// </summary>
        public string groupes { get; set; }

        /// <summary>
        /// 姓名
        /// </summary>
        public string name { get; set; }

        /// <summary>
        /// 性別
        /// </summary>
        public string gender { get; set; }

        /// <summary>
        /// 班別
        /// </summary>
        public string classes { get; set; }

        /// <summary>
        /// 所屬佛院
        /// </summary>
        public string buddhaHall { get; set; }

        /// <summary>
        /// 所屬區別
        /// </summary>
        public string district { get; set; }
    }
}