﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ZcbwComTwApi.VM
{
    /// <summary>
    /// 登入回傳成功回傳模型
    /// </summary>
    public class LoginUserInfoVM
    {
        /// <summary>
        /// 角色
        /// </summary>
        public string roles { get; set; }

        /// <summary>
        /// 使用者名稱
        /// </summary>
        public string userName { get; set; } 
    }

    /// <summary>
    /// 驗證重複帳號回傳模型
    /// </summary>
    public class LoginAccountVM
    {
        /// <summary>
        /// 帳號
        /// </summary>
        public string account { get; set; }
    }

    /// <summary>
    /// 登入令牌
    /// </summary>
    public class LoginUserInfoTokenVM
    {
        /// <summary>
        /// //令牌
        /// </summary>
        public string token { get; set; } 
    }
}