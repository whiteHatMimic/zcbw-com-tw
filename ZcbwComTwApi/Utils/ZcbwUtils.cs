﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Security.Cryptography;
using System.Text;

namespace ZcbwComTwApi.Utils
{
    public class ZcbwUtils
    {
        /// <summary>
        /// sha 加密
        /// </summary>
        /// <param name="data"></param>
        /// <returns></returns>
        public string ZcbwShaEncrypt(string data)
        {
            SHA256Managed sha256 = new SHA256Managed();

            byte[] hash = sha256.ComputeHash(Encoding.UTF8.GetBytes(data));
            string hashString = BitConverter.ToString(hash).Replace("-", "").ToLower();

            return hashString;
        }

        /// <summary>
        /// 取得客戶 ip 位置
        /// </summary>
        /// <returns></returns>
        //public string ZcbwGetHostAddress()
        //{
        //    //string result = string.Empty;

        //    //result = 
            

        //    return result;
        //}
    }
}