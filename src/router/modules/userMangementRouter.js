import Layout from '@/layout'

export const userMangementRouter = {
  path: '/userMangement',                    //使用者資料管理
  redirect: '/userMangement/index',
  component: Layout,
  children: [
    {
      path: 'index',
      component: () => import('@/views/userMangement/index.vue'),
      name: 'userInfoList',
      meta: {
        title: '使用者帳號管理', roles: ['supper']
      }
    },
    {
      path: 'new',
      component: () => import('@/views/userMangement/newUserMangement.vue'),
      name: 'userInfoNew',
      meta: {
        title: '使用者帳號新增', roles: ['supper']
      },
      hidden: true
    },
    {
      path: 'md/:userId',
      component: () => import('@/views/userMangement/mdUserMangement.vue'),
      name: 'userInfoMd',
      meta: {
        title: '使用者帳號更新', roles: ['supper']
      },
      hidden: true
    }
  ]
};


export default userMangementRouter;