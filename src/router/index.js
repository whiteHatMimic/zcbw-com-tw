import Vue from 'vue';
import Router from 'vue-router';

Vue.use(Router);

/** Layout */
import Layout from '@/layout';

/** Router Modules */
import classRoomRouter from './modules/classRoomRouter.js';
import userMangementRouter from './modules/userMangementRouter.js';
import systemPostRouter from './modules/systemPostRouter.js';
import buddhaHallRouter from './modules/buddhaHallRouter.js';

//Base router
export const constantRoutes = [
  {
    path: '/redirect',
    component: Layout,
    children: [
      {
        path: '/redirect/:path(.*)',
        component: () => import('@/views/redirect/index.vue')
      }
    ],
    hidden: true,
  },
  {
    path: '/login',
    component: () => import('@/views/login/index.vue'),
    meta: { title: '登入' },
    hidden: true
  },
  {
    path: '/', //儀錶板
    redirect: 'dashboard/index',
    component: Layout,
    children: [
      {
        path: 'dashboard/index',
        component: () => import('@/views/dashboard/index.vue'),
        name: 'dashboard',
        meta: {
          title: '首頁'
        }
      }
    ]
  },
  {
    path: '/404',    //錯誤頁面
    component: () => import('@/views/errorPage/404.vue'),
    hidden: true
  },
  {
    path: '*',
    component: () => import('@/views/errorPage/404.vue'),
    hidden: true
  }
];

//Permission router
export const asyncRoutes = [
  {
    path: '/reportManagement', //報到資料管理系統
    redirect: '/reportManagement/index',
    component: Layout,
    children: [
      {
        path: 'index',
        component: () => import('@/views/reportManagement/index.vue'),
        name: 'reportManage',
        meta: {
          title: '報到資料管理系統', roles: ['supper']
        }
      }
    ]
  },
  {
    path: '/legislativeCouncilManagement',           //法會資料管理系統
    redirect: '/legislativeCouncilManagement/index',
    component: Layout,
    children: [
      {
        path: 'index',
        component: () => import('@/views/legislativeCouncilManagement/index.vue'),
        name: 'LegislativeCouncilManagement',
        meta: {
          title: '法會資料管理系統', roles: ['supper']
        }
      }
    ]
  },
  {
    path: '/forwardingApplyManagement',                           //轉址申請系統
    redirect: '/forwardingApplyManagement/index',
    component: Layout,
    children: [
      {
        path: 'index',
        component: () => import('@/views/forwardingApplyManagement/index.vue'),
        name: 'forwardingApplyManagement',
        meta: {
          title: '轉址申請系統', roles: ['supper']
        }
      }
    ]
  },
  {
    path: '/pointTransmitterManagement',                    //點傳師資料管理系統
    redirect: '/pointTransmitterManagement/index',
    component: Layout,
    children: [
      {
        path: 'index',
        component: () => import('@/views/pointTransmitterManagement/index.vue'),
        name: 'pointTransmitterManagement',
        meta: {
          title: '點傳師資料管理系統', roles: ['supper']
        }
      }
    ]
  },
  {
    path: '/attecdanceRegistraction',  //出席表登入系統
    redirect: '/attecdanceRegistraction/index',
    component: Layout,
    children: [
      {
        path: 'index',
        component: () => import('@/views/attecdanceRegistraction/index.vue'),
        name: 'attecdanceRegistraction',
        meta: {
          title: '出席表登入系統', roles: ['supper']
        }
      }
    ]
  },
  classRoomRouter,      //班務管理系統
  buddhaHallRouter,     //佛堂管理系統
  userMangementRouter,  //使用者管理
  systemPostRouter,     //系統公告
  {
    path: '/pageMangement',   //頁面管理
    redirect: '/pageMangement/index',
    component: Layout,
    children: [
      {
        path: 'index',
        component: () => import('@/views/pageMangement/index.vue'),
        name: 'pageMangement',
        meta: {
          title: '頁面管理系統', roles: ['supper']
        }
      }
    ]
  },
  {
    path: '/testMenu1',
    redirect: '/testMenu1/test1',
    component: Layout,
    meta: {
      title: 'testMenu1', roles: ['supper']
    },
    children: [
      {
        path: 'test1',
        component: () => import('@/views/test1/test1.vue'),
        name: 'test1',
        meta: {
          title: 'test1', roles: ['supper']
        }
      },
      {
        path: 'test2',
        component: () => import('@/views/test1/test2.vue'),
        name: 'test2',
        meta: {
          title: 'test2', roles: ['supper']
        }
      },
      {
        path: 'test3',
        component: () => import('@/views/test1/test3.vue'),
        name: 'test3',
        meta: {
          title: 'test3', roles: ['supper']
        }
      }
    ]
  },
  {
    path: '/testMenu2',
    redirect: '/testMenu2/test1',
    component: Layout,
    meta: {
      title: 'testMenu2', roles: ['supper']
    },
    children: [
      {
        path: 'test1',
        component: () => import('@/views/test2/test1.vue'),
        name: 'test1',
        meta: {
          title: 'test1', roles: ['supper']
        }
      },
      {
        path: 'test2',
        component: () => import('@/views/test2/test2.vue'),
        name: 'test1',
        meta: {
          title: 'test2', roles: ['supper']
        }
      }
    ]
  },
  {
    path: '/testMenu3',
    redirect: '/testMenu3/test1',
    component: Layout,
    meta: {
      title: 'testMenu3', roles: ['supper']
    },
    children: [
      {
        path: 'test1',
        component: () => import('@/views/test3/test1.vue'),
        name: 'test1',
        meta: {
          title: 'test1', roles: ['supper']
        }
      },
      {
        path: 'test2',
        component: () => import('@/views/test3/test2.vue'),
        name: 'test1',
        meta: {
          title: 'test2', roles: ['supper']
        }
      }
    ]
  }
];

const createRouter = () => new Router({
  scrollBehavior: () => ({ y: 0 }),
  routes: constantRoutes
});

const router = createRouter();

export function resetRouter() {
  const newRouter = createRouter();
  router.matcher = newRouter.matcher; // reset router
}

export default router;
