const getters = {
    userName: state => state.user.userName,
    token: state => state.user.token,
    roles: state => state.user.roles,
    loading: state => state.app.loading,
    sidebar: state => state.app.sidebar,
    device: state => state.app.device,
    permission_routes: state => state.permission.routes
};

export default getters;