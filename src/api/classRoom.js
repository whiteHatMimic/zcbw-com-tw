import request from '@/utils/request.js';

/**
 * 取得班務基本資料
 * @param {string} classes 班別
 * @param {string} gender 性別(乾道|坤道)
 * @param {number} page 頁碼
 * @param {number} itemBarMenu 筆數
 * @returns 
 */
export function GetClassRoomBase(classes, gender, page, itemBarMenu){
    return request({
        url: 'api/FetchClassRoomBase',
        method: 'post',
        data: {
            classes,
            gender,
            page,
            itemBarMenu
        }
    });
}

/**
 * 取得班務詳細資料
 * @param {string} classes 班別
 * @param {string} gender 性別(乾道|坤道)
 * @param {string} name 姓名
 * @param {number} page 頁碼
 * @param {number} itemBarMenu 筆數
 * @returns 
 */
export function GetClassRoomDetail(classes, gender, name, page, itemBarMenu) {
    return request({
        url: 'api/FetchClassRoomDetail',
        method: 'post',
        data: {
            classes,
            gender,
            name,
            page,
            itemBarMenu
        }
    });
}

/**
 * 取得一筆班務資料
 * @param {number} classRoomId 班務 id
 * @returns 
 */
export function GetOnlyClassRoom(classRoomId)
{
    return request({
        url: 'api/FetchOnlyClassRoom',
        method: 'post',
        data: {
            classRoomId
        }
    })
}

/**
 * 新增班務資料
 * @param {object} classRoomForm 班務資料
 * @returns 
 */
export function InsertClassRoom(classRoomForm) {
    return request({
        url: 'api/CreateClassRoom',
        method: 'post',
        data: {
            groupes: classRoomForm.groupes,
            groupNo: classRoomForm.groupNo,
            serialNo: classRoomForm.serialNo,
            classLocation: classRoomForm.classLocation,
            name: classRoomForm.name,
            gender: classRoomForm.gender,
            classes: classRoomForm.classes,
            buddhaHall: classRoomForm.buddhaHall,
            district: classRoomForm.district,
            nowUser: classRoomForm.nowUser
        }
    });
}

/**
 * 更新班務資料
 * @param {object} classRoomForm 班務資料
 * @returns 
 */
export function UpdateClassRoom(classRoomForm) {
    return request({
        url: 'api/ReplaceClassRoom',
        method: 'patch',
        data: {
            classRoomId: classRoomForm.classRoomId,
            classLocation: classRoomForm.classLocation,
            groupes: classRoomForm.groupes,
            groupNo: classRoomForm.groupNo,
            serialNo: classRoomForm.serialNo,
            name: classRoomForm.name,
            gender: classRoomForm.gender,
            classes: classRoomForm.classes,
            buddhaHall: classRoomForm.buddhaHall,
            district: classRoomForm.district,
            mUser: classRoomForm.mUser
        }
    });
}

/**
 * 刪除班務資料
 * @param {number} classRoomId 班務 id
 * @returns 
 */
export function DeleteClassRoom(classRoomId) {
    return request({
        url: 'api/CancelClassRoom',
        method: 'delete',
        params: {
            classRoomId
        }
    });
}

/**
 * 匯出班務表
 * @param {string} classes 班別
 * @param {string} gender 性別
 * @param {string} name 名稱
 */
export function ReportClassRoom(classes, gender, name)
{
    return request({
        url: 'api/ReportClassRoom',
        method: 'post',
        data: {
            classes,
            gender,
            name
        },
        responseType: 'blob'
    })
}