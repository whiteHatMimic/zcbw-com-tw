import request from '@/utils/request.js';

/**
 * 取得使用者帳號角色
 * @param {string} roles 平台角色
 * @param {string} type 搜尋類型
 * @param {string} page 頁籤
 * @param {string} itemBarMenu 筆數
 */
export function GetUserInfoBase(roles, status, page, itemBarMenu) {
    return request({
        url: 'api/FetchUserInfoBase',
        method: 'post',
        data: {
            roles,
            status,
            page,
            itemBarMenu
        }
    });
}

/**
 * 取得使用者詳細資料，加以使用帳號、使用者名稱搜尋
 * @param {string} roles 平台角色
 * @param {string} type 搜尋類型: 帳號、姓名
 * @param {string} page 頁碼
 * @param {string} itemBarMenu 筆數
 */
export function GetUserInfoDetail(roles, status, type, page, itemBarMenu) {
    return request({
        url: 'api/FetchUserInfoDetail',
        method: 'post',
        data: {
            roles,
            status,
            type,
            page,
            itemBarMenu
        }
    });
}


/**
 * 取得一筆使用者資訊
 * @param {string} userId 使用者 id
 */
export function GetOnlyUserInfo(userId) {
    return request({
        url: 'api/FetchOnlyUserInfo',
        method: 'post',
        data: {
            userId
        }
    });
}

/**
 * 新增使用者資訊
 * @param {object} userfromInfo 使用者表單
 */
export function InsertUserInfo(userInfoForm) {
    return request({
        url: 'api/CreateUserInfo',
        method: 'post',
        data: {
            userName: userInfoForm.userName,
            roles: userInfoForm.roles,
            account: userInfoForm.account,
            password: userInfoForm.password,
            email: userInfoForm.email,
            phone: userInfoForm.phone,
            status: userInfoForm.status,
            nowUser: userInfoForm.nowUser
        }
    });
}

/**
 * 更新使用者帳號資訊
 * @param {object} userInfoForm 使用者表單
 * @returns 
 */
export function UpdateUserInfo(userInfoForm) {
    return request({
        url: 'api/ReplaceUserInfo',
        method: 'patch',
        data: {
            userId: userInfoForm.userId,
            userName: userInfoForm.userName,
            roles: userInfoForm.roles,
            email: userInfoForm.email,
            phone: userInfoForm.phone,
            status: userInfoForm.status,
            mUser: userInfoForm.mUser
        }
    });
}

/**
 * 刪除使用者帳號
 * @param {number} userId 使用者 id
 */
export function DeleteUserInfo(userId)
{
    return request({
        url: 'api/CancelUserInfo',
        method: 'delete',
        params: {
            userId
        }
    });
}