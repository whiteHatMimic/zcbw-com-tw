import request from '@/utils/request.js';

/**
 * 取得系統公告基本資訊
 * @param {string} publish 發佈狀態
 * @param {string} page 頁碼
 * @param {string} itemBarMenu 筆數
 */
export function GetSystemPostBase(publish, page, itemBarMenu) {
    return request({
        url: 'api/FetchSystemPostBase',
        method: 'post',
        data: {
            publish,
            page,
            itemBarMenu
        }
    });
}

/**
 * 取得系統公告詳細資訊
 * @param {string} publish 發佈狀態
 * @param {string} postTitle 標題
 * @param {string} page 頁碼
 * @param {string} itemBarMenu 筆數
 */
export function GetSystemPostDetail(publish, postTitle, page, itemBarMenu) {
    return request({
        url: 'api/FetchSystemPostDetail',
        method: 'post',
        data: {
            publish,
            postTitle,
            page,
            itemBarMenu
        }
    });
}

/** 
 * 取得一筆系統公告
 * @param {number} systemPostId 系統公告 id
 */
export function GetOnlySystemPost(systemPostId) {
    return request({
        url: 'api/FetchOnlySystemPost',
        method: 'post',
        data: {
            systemPostId: systemPostId
        }
    });
}

/**
 * 新增系統公告
 * @param {object} systemPostForm 系統公告表單
 */
export function CreateSystemPost(systemPostForm) {
    return request({
        url: 'api/CreateSystemPost',
        method: 'post',
        data: {
            postTitle: systemPostForm.postTitle,
            postMessage: systemPostForm.postMessage,
            publish: systemPostForm.publish,
            nowUser: systemPostForm.nowUser
        }
    });
}

/**
 * 更新系統公告
 * @param {string} systemPost 系統公告表單
 */
export function UpdateSystemPost(systemPostForm) {
    return request({
        url: 'api/ReplaceSystemPost',
        method: 'patch',
        data: {
            systemPostId: systemPostForm.systemPostId,
            postTitle: systemPostForm.postTitle,
            postMessage: systemPostForm.postMessage,
            publish: systemPostForm.publish,
            mUser: systemPostForm.mUser
        }
    });
}

/**
 * 更新系統公告發佈狀態
 * @param {number} systemPostId 系統公告 id
 * @param {string} publish 發佈狀態
 * @param {string} userName 建立使用者
 */
export function UpdateSystemPostPulish(systemPostId, publish, userName) {
    return request({
        url: 'api/ReplaceSystemPostPublishStatus',
        method: 'patch',
        data: {
            systemPostId: systemPostId,
            publish: publish,
            mUser: userName
        }
    });
}

/**
 * 刪除系統公告
 * @param {number} 系統公告 id
 */
export function DeleteSystemPost(systemPostId) {
    return request({
        url: 'api/CancelSystemPost',
        method: 'delete',
        params: {
            systemPostId
        }
    });
}