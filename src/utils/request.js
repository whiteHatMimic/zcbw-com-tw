import Vue from 'vue';
import axios from 'axios';
import VueAxios from 'vue-axios';
import qs from 'qs';
import store from '@/store';
import { getToken } from '@/utils/auth';

Vue.use(VueAxios, axios);

const service = axios.create({
    baseURL: 'http://localhost:8748/',
    timeout: 5000
}); //將 axios 儲存至變數


service.interceptors.request.use(
    config => {
        store.dispatch('app/changeLoading', true);

        if (store.getters.token)
        {
            if (config.method === 'post' || config.method === 'POST')
            {
                config.data = qs.stringify(config.data);
            }

            if (config.method === 'patch' || config.method === 'PATCH')
            {
                config.data = qs.stringify(config.data);
            }
            
            getToken() && (config.headers.Authorization = `Bearer ${getToken()}`);
        }
        
        return config;
    },
    error => {
        console.log(error);
        return new Promise.reject(error);
    }
)

service.interceptors.response.use( //使用 axios 攔截器，攔截 response 回傳所有結果
    response => {
        store.dispatch('app/changeLoading', false);
        
        if (response.request.readyState === 4) //如果攔截回來有產生標頭，且 status code 狀態為 200 成功狀態
        {
            return Promise.resolve(response); //回傳 response 資料
        }
    },
    error => {
        // let serverErrorMessage = {
        //     message: null
        // }; //儲存 api 發生什麼錯誤

        // if (error && error.response)  //如果 error 和 error.response 存在時
        // {
        //     switch(error.response.status)  //判斷 status code 錯誤狀態
        //     {
        //         case 401:                                         //如果遇到 401 狀態
        //             serverErrorMessage.message = '401';          // 儲存 401 ERROR 陣列
        //             break;                                            //中止
        //         case 403:                                         //如果遇到 403 狀態
        //             serverErrorMessage.message = '403';          //儲存 401 ERROR 陣列
        //             break;                                            //中止
        //         case 404:                                         //如果遇到 404 狀態
        //             serverErrorMessage.message = '找不到頁面';    // 儲存 404 ERROR 陣列
        //             break;                                            //中止
        //         case 500:                                         //如果遇到 500 狀態
        //             serverErrorMessage.message = '伺服器錯誤';    //儲存 500 ERROR 陣列
        //             break;                                            //中止
        //         case 503:                                         //如果遇到 503 狀態
        //             serverErrorMessage.message = '服務失敗';      //儲存 503 ERROR 陣列
        //             break;                                            //中止
        //         default:                                          //其他
        //             serverErrorMessage.message = '連接錯誤';      //回傳 "連接錯誤" ERROR 陣列
        //             break;
        //     }
        // }
        // else  //否則什麼都沒有
        // {
        //     serverErrorMessage.message = '連接服務失敗';   //回傳 "連接服務失敗" ERROR 陣列
        // }

        store.dispatch('app/changeLoading', false);

        return Promise.reject(error.response);   //回傳 serverErrorMessage 變數，最終錯誤訊息
});

export default service;