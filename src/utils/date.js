export function FormatDate(date) {
    return `${date.substr(0, 4)}/${date.substr(4, 2)}/${date.substr(6, 2)}`
}

export function FormatTime(time) {
    return `${time.substr(0, 2)}:${time.substr(2, 2)}`
}