<p align="center">
    <h1>正承佛院資訊系統<h1>
</p>

<p align="center">
    <a href="https://vuejs.org/v2/guide/">
        <img src="https://img.shields.io/badge/vue-4.5.14-blue">
    </a>
    <a href="https://dev.mysql.com/downloads/installer/">
        <img src="https://img.shields.io/badge/mysql-8.0.26-blue">
    </a>
    <a href="https://getbootstrap.com/docs/4.6/getting-started/introduction/">
        <img src="https://img.shields.io/badge/bootstrap-4.6.0-blue">
    </a>
    <a href="javascript:void">
        <img src="https://img.shields.io/badge/webapi-4.7.2-blue">
    </a>
</p>

## 簡介
正承佛院資訊系統是一個後台解決方案

## 功能

```textile
登入/註銷
首頁
報到管理系統
法會管理系統
轉址申請系統
點傳師管理系統
班務管理系統
使用者帳號管理
系統公告
```

## 開發

```git
#克隆項目
git clone https://gitlab.com/whiteHatMimic/zcbw-com-tw.git

#進入目錄
cd zcbw-com-tw

#安裝依賴
npm install

#啟動服務
npm serve
```

## 發佈

```git
#打包檔案
npm build
```
